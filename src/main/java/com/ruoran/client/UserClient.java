package com.ruoran.client;

import com.ruoran.common.Result;
import com.ruoran.user.domain.User;
import com.ruoran.user.util.Page;

import java.util.List;

public interface UserClient {

    Result<Page<User>> queryForPage(int pageNum, int pageSize);

    Result<List<User>> queryList(int pageNum, int pageSize);

    Result<User> add(User user);

    Result<User> delete(Long id);

    Result<User> update(User user);

    Result<User> find(Long id);

    Result<List<User>> find(List<Long> ids);

    Result<User> login(User user);

    Result<Long> count();
}
