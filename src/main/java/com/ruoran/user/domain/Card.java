package com.ruoran.user.domain;

import java.io.Serializable;
import java.util.Date;

public class Card implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer id;
    private String number;
    private Integer type;
    private String name;
    private Date issueDate;
    private Date expiringDate;

    public Card() {
    }

    public Card(Integer type, String name, Date startTime) {
        super();
        this.type = type;
        this.name = name;
        this.issueDate = startTime;
    }

    public Card(String number, Integer type, String name, Date startTime) {
        super();
        this.number = number;
        this.type = type;
        this.name = name;
        this.issueDate = startTime;
    }

    public Card(Integer id, String number, Integer type, String name, Date startTime) {
        super();
        this.id = id;
        this.number = number;
        this.type = type;
        this.name = name;
        this.issueDate = startTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(Date issueDate) {
        this.issueDate = issueDate;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Date getExpiringDate() {
        return expiringDate;
    }

    public void setExpiringDate(Date expiringDate) {
        this.expiringDate = expiringDate;
    }

    @Override
    public String toString() {
        return "Card{" +
                "id=" + id +
                ", number='" + number + '\'' +
                ", type=" + type +
                ", name='" + name + '\'' +
                ", issueDate=" + issueDate +
                ", expiringDate=" + expiringDate +
                '}';
    }
}
