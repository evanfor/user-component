package com.ruoran.user.domain;

import com.ruoran.user.xml.adapter.InstantAdapter;
import com.ruoran.user.xml.adapter.LocalDateTimeAdapter;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Date;

public class User implements Serializable {

    private Long id;
    private String name;
    private String username;
    private String password;
    private Integer age;
    private Boolean married;
    private Double money;
    private Short gender;
    private Float salary;
    private Date birthday;
    private LocalDateTime createTime;
    private Instant updateTime;
    private BigDecimal lng;
    private BigDecimal lat;
    private Card card;

    public User(String username, String password, String name, Integer age, Boolean married, Double money, Short gender, Float salary, Date birthday, LocalDateTime createTime) {
        this.username = username;
        this.password = password;
        this.name = name;
        this.age = age;
        this.married = married;
        this.money = money;
        this.gender = gender;
        this.salary = salary;
        this.birthday = birthday;
        this.createTime = createTime;
        this.updateTime = Instant.now();
    }

    public User(String name, String username, String password, Integer age, Boolean married, Double money, Short gender, Float salary, Date birthday, LocalDateTime createTime, BigDecimal lng, BigDecimal lat) {
        this.name = name;
        this.username = username;
        this.password = password;
        this.age = age;
        this.married = married;
        this.money = money;
        this.gender = gender;
        this.salary = salary;
        this.birthday = birthday;
        this.createTime = createTime;
        this.updateTime = Instant.now();
        this.lng = lng;
        this.lat = lat;
    }

    public User(Long id, String username, String password, String name, Boolean married, Double money, Short gender, Float salary) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.name = name;
        this.married = married;
        this.money = money;
        this.gender = gender;
        this.salary = salary;
        this.createTime = LocalDateTime.now();
        this.updateTime = Instant.now();
    }

    public User(Long id, String username, String password, String name, Integer age, Boolean married, Double money, Short gender, Float salary, Date birthday, LocalDateTime createTime) {
        this(username, password, name, age, married, money, gender, salary, birthday, createTime);
        this.id = id;
        this.updateTime = Instant.now();
    }

    public User(Long id, String username, String password, String name, Integer age, Boolean married, Double money, Short gender, Float salary, Date birthday, LocalDateTime createTime, BigDecimal lng, BigDecimal lat) {
        this(username, password, name, age, married, money, gender, salary, birthday, createTime, lng, lat);
        this.id = id;
        this.updateTime = Instant.now();
    }

    public User() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Boolean getMarried() {
        return married;
    }

    public void setMarried(Boolean married) {
        this.married = married;
    }

    public Double getMoney() {
        return money;
    }

    public void setMoney(Double money) {
        this.money = money;
    }

    public Short getGender() {
        return gender;
    }

    public void setGender(Short gender) {
        this.gender = gender;
    }

    public Float getSalary() {
        return salary;
    }

    public void setSalary(Float salary) {
        this.salary = salary;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    @XmlJavaTypeAdapter(LocalDateTimeAdapter.class)
    @XmlElement(name = "createTime", required = true)
    public LocalDateTime getCreateTime() {
        return createTime;
    }


    public void setUpdateTime(Instant updateTime) {
        this.updateTime = updateTime;
    }

    @XmlJavaTypeAdapter(InstantAdapter.class)
    @XmlElement(name = "updateTime", required = true)
    public Instant getUpdateTime() {
        return updateTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public BigDecimal getLat() {
        return lat;
    }

    public void setLat(BigDecimal lat) {
        this.lat = lat;
    }

    public BigDecimal getLng() {
        return lng;
    }

    public void setLng(BigDecimal lng) {
        this.lng = lng;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", age=" + age +
                ", married=" + married +
                ", money=" + money +
                ", gender=" + gender +
                ", salary=" + salary +
                ", birthday=" + birthday +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", lng=" + lng +
                ", lat=" + lat +
                ", card=" + card +
                '}';
    }
}
