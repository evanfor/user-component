package com.ruoran.user.service;


import java.io.*;
import java.text.ParseException;

public interface UserExtService {

    void exportUsers(int pageNum, int pageSize, int export, OutputStream outputStream) throws IOException;

    void importUsers(InputStream inputStream) throws IOException, ParseException;

}
