package com.ruoran.user.service;

import com.ruoran.user.domain.User;
import com.ruoran.user.util.Page;

import java.util.List;

public interface UserService {

    Page<User> queryForPage(int pageNum, int pageSize);

    List<User> queryList(int pageNum, int pageSize);

    User add(User user);

    User delete(Long id);

    User update(User user);

    User find(Long id);

    List<User> find(List<Long> ids);

    User login(User user);

    Long count();

    User queryByUsername(String username);
}
