package com.ruoran.user.service.impl;

import cn.idev.excel.FastExcel;
import cn.idev.excel.context.AnalysisContext;
import cn.idev.excel.event.AnalysisEventListener;
import cn.idev.excel.metadata.data.ReadCellData;
import cn.idev.excel.support.ExcelTypeEnum;
import com.ruoran.user.dao.UserDAO;
import com.ruoran.user.domain.User;
import com.ruoran.user.excel.dto.ExcelUser;
import com.ruoran.user.service.UserExtService;
import com.ruoran.user.util.Page;

import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class UserExtServiceImpl implements UserExtService {

    private UserDAO userDAO;

    public void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    public UserExtServiceImpl(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    public void exportUsers(int pageNum, int pageSize, int exportAll, OutputStream outputStream) {
        Page<User> data = exportAll == 0 ? userDAO.queryForPage(pageNum, pageSize) : userDAO.queryForAll();

        List<ExcelUser> list = data.getResult().stream().map(user -> {
            ExcelUser excelUser = new ExcelUser();
            excelUser.setId(user.getId());
            excelUser.setName(user.getName());
            excelUser.setUsername(user.getUsername());
            excelUser.setPassword(user.getPassword());
            excelUser.setAge(user.getAge());
            excelUser.setMarried(user.getMarried());
            excelUser.setMoney(user.getMoney());
            excelUser.setGender(user.getGender());
            excelUser.setSalary(user.getSalary());
            excelUser.setBirthday(user.getBirthday());
            excelUser.setCreateTime(user.getCreateTime());
            excelUser.setUpdateTime(user.getUpdateTime());
            excelUser.setLng(user.getLng());
            excelUser.setLat(user.getLat());
            return excelUser;
        }).collect(Collectors.toList());

        FastExcel.write(outputStream, ExcelUser.class)
                .excelType(ExcelTypeEnum.XLSX)
                .autoCloseStream(true)
                .charset(StandardCharsets.UTF_8)
                .sheet("用户信息")
                .useDefaultStyle(true)
                .doWrite(list);
    }

    public void importUsers(InputStream inputStream) {
        FastExcel.read(inputStream, ExcelUser.class, new AnalysisEventListener<ExcelUser>() {
            private final List<ExcelUser> dataList = new ArrayList<>();

            @Override
            public void invoke(ExcelUser excelUser, AnalysisContext analysisContext) {
                dataList.add(excelUser);
            }

            @Override
            public void invokeHead(Map<Integer, ReadCellData<?>> headMap, AnalysisContext context) {
                super.invokeHead(headMap, context);
            }

            @Override
            public void doAfterAllAnalysed(AnalysisContext analysisContext) {
                System.out.println(dataList.size());
            }
        }).doReadAllSync();
    }
}
