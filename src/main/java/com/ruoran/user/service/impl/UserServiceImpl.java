package com.ruoran.user.service.impl;

import com.ruoran.user.CountChangeListener;
import com.ruoran.user.dao.UserDAO;
import com.ruoran.user.domain.User;
import com.ruoran.user.service.UserService;
import com.ruoran.user.util.Page;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private UserDAO userDAO;

    public UserServiceImpl(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    public Page<User> queryForPage(int pageNum, int pageSize) {
        return userDAO.queryForPage(pageNum, pageSize);
    }

    @Override
    public List<User> queryList(int pageNum, int pageSize) {
        return userDAO.queryList(pageNum, pageSize);
    }

    public User find(Long id) {
        return userDAO.find(id);
    }

    @Override
    public List<User> find(List<Long> ids) {
        return userDAO.find(ids);
    }

    public User add(User user) {
        return userDAO.add(user);
    }

    public User delete(Long id) {
        return userDAO.delete(id);
    }

    public User update(User user) {
        return userDAO.update(user);
    }

    @Override
    public Long count() {
        return userDAO.count();
    }

    @Override
    public User queryByUsername(String username) {
        return userDAO.queryByUsername(username);
    }

    @Override
    public User login(User user) {
        return userDAO.login(user);
    }

    public void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    public void setCountChangeListener(CountChangeListener listener) {
        this.userDAO.setCountChangeListener(listener);
    }
}
