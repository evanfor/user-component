package com.ruoran.user.excel.dto;

import cn.idev.excel.annotation.ExcelProperty;
import cn.idev.excel.annotation.write.style.ColumnWidth;
import cn.idev.excel.annotation.write.style.HeadRowHeight;
import com.ruoran.user.excel.converter.BigDecimalConverter;
import com.ruoran.user.excel.converter.InstantConverter;
import com.ruoran.user.excel.converter.MarriedConverter;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Date;

@Getter
@Setter
@ToString
@HeadRowHeight(30)
public class ExcelUser implements Serializable {

    @ExcelProperty(value = "ID")
    private Long id;
    @ExcelProperty(value = "姓名")
    private String name;
    @ExcelProperty(value = "用户名")
    private String username;
    @ExcelProperty(value = "密码")
    private String password;
    @ExcelProperty(value = "年龄")
    private Integer age;
    @ExcelProperty(value = "已婚", converter = MarriedConverter.class)
    private Boolean married;
    @ExcelProperty(value = "资产")
    private Double money;
    @ExcelProperty(value = "性别")
    private Short gender;
    @ExcelProperty(value = "工资")
    private Float salary;
    @ExcelProperty(value = "生日")
    private Date birthday;
    @ExcelProperty(value = "创建时间")
    @ColumnWidth(20)
    private LocalDateTime createTime;
    @ExcelProperty(value = "更新时间", converter = InstantConverter.class)
    @ColumnWidth(20)
    private Instant updateTime;
    @ExcelProperty(value = "经度", converter = BigDecimalConverter.class)
    private BigDecimal lng;
    @ExcelProperty(value = "纬度", converter = BigDecimalConverter.class)
    private BigDecimal lat;
}
