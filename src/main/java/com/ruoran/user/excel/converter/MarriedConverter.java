package com.ruoran.user.excel.converter;

import cn.idev.excel.converters.Converter;
import cn.idev.excel.converters.WriteConverterContext;
import cn.idev.excel.metadata.GlobalConfiguration;
import cn.idev.excel.metadata.data.ReadCellData;
import cn.idev.excel.metadata.data.WriteCellData;
import cn.idev.excel.metadata.property.ExcelContentProperty;

public class MarriedConverter implements Converter<Boolean> {

    @Override
    public WriteCellData<?> convertToExcelData(WriteConverterContext<Boolean> context) {
        return new WriteCellData<String>(context.getValue() ? "是" : "否");
    }

    @Override
    public Boolean convertToJavaData(ReadCellData<?> cellData, ExcelContentProperty contentProperty, GlobalConfiguration globalConfiguration) {
        return cellData.getStringValue().equals("是");
    }
}
