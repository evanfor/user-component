package com.ruoran.user.excel.converter;

import cn.idev.excel.converters.Converter;
import cn.idev.excel.converters.WriteConverterContext;
import cn.idev.excel.metadata.GlobalConfiguration;
import cn.idev.excel.metadata.data.ReadCellData;
import cn.idev.excel.metadata.data.WriteCellData;
import cn.idev.excel.metadata.property.ExcelContentProperty;

import java.time.Instant;

public class InstantConverter implements Converter<Instant> {

    @Override
    public Instant convertToJavaData(ReadCellData<?> cellData, ExcelContentProperty contentProperty, GlobalConfiguration globalConfiguration) {
        return Instant.ofEpochMilli(Long.parseLong(cellData.getStringValue().trim()));
    }

    @Override
    public WriteCellData<String> convertToExcelData(WriteConverterContext<Instant> context) {
        return new WriteCellData<>(String.valueOf(context.getValue().toEpochMilli()));
    }
}
