package com.ruoran.enums;

public enum CardType {
    ID_CARD(1, "二代身份证");

    private final int type;
    private final String name;

    CardType(int type, String name) {
        this.type = type;
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public String getName() {
        return name;
    }
}
