package com.ruoran.user.service;

import com.ruoran.user.dao.UserDAO;
import com.ruoran.user.service.impl.UserExtServiceImpl;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.text.ParseException;

public class UserExtServiceTest {

    private final UserDAO userDAO = new UserDAO();
    private final UserExtService userExtService = new UserExtServiceImpl(userDAO);

    @Test
    public void testExport() throws IOException {
        File file = new File("user.xlsx");
        userExtService.exportUsers(1, 10, 0, Files.newOutputStream(file.toPath()));
    }

    @Test
    public void testImport() throws IOException, ParseException {
        File file = new File("user.xlsx");
        userExtService.importUsers(Files.newInputStream(file.toPath()));
    }
}
